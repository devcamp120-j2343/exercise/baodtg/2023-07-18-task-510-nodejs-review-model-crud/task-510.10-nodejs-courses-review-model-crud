//B1: khai báo thu viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema của mongoose;
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:
const reviewSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    // _id: {
    //     type:mongoose.Types.ObjectId
    // }
    stars: {
        type: Number,
        default: 0
    },
   note: {
    type: String,
    required: false
   }
  

});
//B4: export schema ra model
module.exports = mongoose.model('review', reviewSchema)