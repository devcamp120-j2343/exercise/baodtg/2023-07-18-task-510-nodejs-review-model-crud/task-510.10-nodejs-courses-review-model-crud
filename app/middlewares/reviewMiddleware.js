const getAllReviewMiddleware = (req, res, next) => {
    console.log("Get All Courses!");
    next();
}

const getReviewMiddleware = (req, res, next) => {
    console.log("Get Courses!");
    next();
}

const postReviewMiddleware = (req, res, next) => {
    console.log("Create Courses!");
    next();
}

const putReviewMiddleware = (req, res, next) => {
    console.log("Update Courses!");
    next();
}

const deleteReviewMiddleware = (req, res, next) => {
    console.log("Delete Courses!");
    next();
}

module.exports = {
    getAllReviewMiddleware,
    getReviewMiddleware,
    postReviewMiddleware,
    putReviewMiddleware,
    deleteReviewMiddleware,
}