//Khai báo thư viện expres
const express = require('express');

//khai báo middleware
const {
    getAllReviewMiddleware,
    getReviewMiddleware,
    postReviewMiddleware,
    putReviewMiddleware,
    deleteReviewMiddleware,
} = require('../middlewares/reviewMiddleware');

//import review controllers:
const { createReview, getAllReviewsOfCourse, getReviewByReviewId, updateReviewById, deleteReviewById } = require('../controllers/review.controller');

//tạo ra router:
const reviewRouter = express.Router();
reviewRouter.use(getAllReviewMiddleware);

reviewRouter.get('/reviews', getAllReviewsOfCourse)
reviewRouter.get('/reviews/:reviewId', getReviewByReviewId)

reviewRouter.post('/reviews', createReview)

reviewRouter.put('/reviews/:reviewId', updateReviewById)

reviewRouter.delete('/reviews/:reviewId', deleteReviewById)


module.exports = {reviewRouter}