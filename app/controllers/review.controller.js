//import thư viện mongoose
const mongoose = require('mongoose');

//import review model:
const reviewModel = require('../models/reviewModel');

//import course model:
const courseModel = require('../models/courseModel')

//coust create review
const createReview = async (req, res) => {
    //B1: Chuẩn bị dữ liệu:
    const {
        courseId,
        stars,
        note
    } = req.body
    //B2: validate dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            "status": "Bad request",
            "message": "Course Id is not valid"
        })
    }
    //Star là số nguyên, lớn hơn 0 và nhỏ hơn 6
    //Number.isInteger(stars) && stars > 0 && stars < 6
    if (stars == undefined || !(Number.isInteger(stars) && stars > 0 && stars < 6)) {
        return res.status(400).json({
            "status": "Bad request",
            "message": "Star is not valid"
        })
    }
    //B3: Thao tác CSDL:
    var newReview = {
        _id: new mongoose.Types.ObjectId(),
        stars: stars,
        note: note
    }
    try {
        const createdReview = await reviewModel.create(newReview);

        const updatedCourse = await courseModel.findByIdAndUpdate(courseId, {
            $push: { reviews: createdReview._id }
        })

        return res.status(201).json({
            status: "Create review successfully",
            course: updatedCourse,
            data: createdReview
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

//Get all reviews of course:
const getAllReviewsOfCourse = async (req, res) => {
    //B1: thu thập dữ liệu:
    const courseId = req.query.courseId;
    //B1: validate data:
    if (courseId !== undefined && !mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            "status": "Bad request",
            "message": "Course ID is not valid"
        })
    }

    //B3: Thực thi model:
    try {
        if (courseId === undefined) {
            const reviewList = await reviewModel.find();
            if (reviewList && reviewList.length > 0) {
                return res.status(200).json({
                    status: "Get all reviews successfully",
                    data: reviewList
                })
            }
        } else {
            const courseInfo = await courseModel.findById(courseId).populate("reviews");
            return res.status(200).json({
                status: "Get all reviews of course successfully",
                data: courseInfo.reviews
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Get reviews by reviewId:
const getReviewByReviewId = async (req, res) => {
    //B1: thu thập dữ liệu:
    let reviewId = req.params.reviewId

    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Review Id is invalid!"
        })
    }
    //B3: thực thi model:
    try {
        const reviewInfo = await reviewModel.findById(reviewId);
        if (reviewInfo) {
            return res.status(200).json({
                status: "Get review by id successfully",
                data: reviewInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//UpdateReview by id
const updateReviewById = (req, res) => {
    //B1: thu thập dữ liệu:
    let reviewId = req.params.reviewId;
    const { stars, note } = req.body;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Review Id in invalid!`
        })
    }

    if (stars !== undefined && !(Number.isInteger(stars) && stars > 0 && stars < 6)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Star is invalid!`
        })
    }
    //B3: thực thi model:

    let updateReviewData = {
        stars,
        note
    }
    reviewModel.findByIdAndUpdate(reviewId, updateReviewData)
        .then((updatedReview) => {
            if (updatedReview) {
                return res.status(200).json({
                    status: "Update review successfully",
                    data: updatedReview
                })
            } else {
                return res.status(404).json({
                    status: `Not found any review`
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//Delete review by id:
const deleteReviewById = async (req, res) => {
    //B1:thu thập dữ liệu
    let reviewId = req.params.reviewId
    // Nếu muốn xóa id thừa trong mảng review thì có thể truyền thêm coureId để xóa
    var courseId = req.query.courseId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Review Id is invalid!`
        })
    }
    try {
        const deletedReview = await reviewModel.findByIdAndDelete(reviewId)
        //Nếu có courseId thì xóa thêm (optional)
        if (courseId !== undefined) {
            await courseModel.findByIdAndUpdate(courseId, {
                $pull: { reviews: reviewId }
            })
        }
        if (deletedReview) {
            return res.status(204).json({
                status: "Delete review by Id successfully",
                data: deletedReview
            })

        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}


module.exports = {
    createReview,
    getAllReviewsOfCourse,
    getReviewByReviewId,
    updateReviewById,
    deleteReviewById

}