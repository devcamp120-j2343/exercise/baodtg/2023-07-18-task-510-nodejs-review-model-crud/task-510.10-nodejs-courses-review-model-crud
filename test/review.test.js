const expect = require("chai").expect;
const request = require("supertest");
const reviewModel = require("../app/models/reviewModel");
const courseModel = require("../app/models/courseModel");

const app = require("../index");
const mongoose = require('mongoose');

let reviewId = '';

describe("/", () => {
    before(async () => {
        await reviewModel.deleteMany({});
    });

    after(async () => {
        // await courseModel.deleteMany({});

        mongoose.disconnect();
    });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course");
    });

    describe("POST /", () => {
        it("should return review when the all request body is valid", async () => {
            const course = await courseModel.findOne().skip(0);

            console.log(course)
            const res = await request(app)
                .post("/reviews")
                .send({

                    courseId: course._id,
                    stars: 5,
                    note: "Very good"
                });
            const data = res.body;
            console.log(data)
            expect(res.status).to.equal(201);
            expect(data.course).to.have.property("_id", course._id.toString());
            expect(data.data).to.have.property("stars", 5);
            expect(data.data).to.have.property("note", "Very good");
        });
    });

    describe("GET /", () => {
        it("should return all reviews", async () => {

            const review = [
                { stars: 5, note: "Very Good" },
                { stars: 4, note: "Good" }
            ];
            await reviewModel.insertMany(review);
            const res = await request(app).get("/reviews");
            console.log(res.body)
            expect(res.status).to.equal(200);
            // expect(res.body.length).to.equal(2);
        });
    });

    describe("GET/:id", () => {
        it("should return a review if valid id is passed", async () => {
            const review = new reviewModel({
                _id: new mongoose.Types.ObjectId(),
                stars: 3,
                note: "Not bad"
            });
            await review.save();
            const res = await request(app).get("/reviews/" + review._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("stars", 3);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/reviews/1");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/reviews/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("PUT /:id", () => {
        it("should update the existing review and return 200", async () => {
            const review = new reviewModel({
                _id: new mongoose.Types.ObjectId(),
                stars: 3,
                note: "Not bad"
            });
            await review.save();

            const res = await request(app)
                .put("/reviews/" + review._id)
                .send({
                    stars: 2,
                    note: "Bad"
                });


            expect(res.status).to.equal(200);
            const reviewUpdated = await reviewModel.findOne({ _id: review._id });
            console.log(reviewUpdated)
            expect(reviewUpdated).to.have.property("stars", 2);
            expect(reviewUpdated).to.have.property("note", "Bad");
        });
    });

    describe("DELETE /:id", () => {
        it("should delete requested id and return response 200", async () => {
            const review = new reviewModel({
                _id: new mongoose.Types.ObjectId(),
                stars: 3,
                note: "Not bad"
            });
            await review.save();
            reviewId = review._id;
            const res = await request(app).delete("/reviews/" + reviewId);
            expect(res.status).to.be.equal(204);
        });

        it("should return 404 when deleted course is requested", async () => {
            let res = await request(app).get("/reviews/" + reviewId);
            expect(res.status).to.be.equal(404);
        });
    });
})  