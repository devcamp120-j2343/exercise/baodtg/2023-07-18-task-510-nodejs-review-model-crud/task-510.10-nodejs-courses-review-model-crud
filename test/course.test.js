const expect = require("chai").expect;
const request = require("supertest");
const courseModel = require("../app/models/courseModel");
const app = require("../index");
const mongoose = require('mongoose');

let courseId = '';

describe("/", () => {
  before(async () => {
    await courseModel.deleteMany({});
  });

  after(async () => {

    // mongoose.disconnect();
  });

  it("should connect and disconnect to mongodb", async () => {
    // console.log(mongoose.connection.states);
    mongoose.disconnect();
    mongoose.connection.on('disconnected', () => {
      expect(mongoose.connection.readyState).to.equal(0);
    });
    mongoose.connection.on('connected', () => {
      expect(mongoose.connection.readyState).to.equal(1);
    });
    mongoose.connection.on('error', () => {
      expect(mongoose.connection.readyState).to.equal(99);
    });

    await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course");
  });



  describe("POST /", () => {
    it("should return course when the all request body is valid", async () => {
      const res = await request(app)
        .post("/courses")
        .send({
          title: "JavaScript",
          description: "Học JS",
          noStudent: 10
        });
      const data = res.body;
      console.log(data)
      expect(res.status).to.equal(201);
      expect(data.data).to.have.property("_id");
      expect(data.data).to.have.property("title", "JavaScript");
      expect(data.data).to.have.property("description", "Học JS");
      expect(data.data).to.have.property("noStudent", 10);

      const course = await courseModel.findOne({ title: "JavaScript" });
      expect(course.title).to.equal('JavaScript');
      expect(course.noStudent).to.equal(10);

    });
  });

  describe("GET /", () => {
    it("should return all courses", async () => {
      const courses = [
        { title: "NodeJS", description: "Học NodeJS", noStudent: 10 },
        { title: "Css", description: "Học css", noStudent: 10 }
      ];
      await courseModel.insertMany(courses);
      const res = await request(app).get("/courses");
      console.log(res.body)
      expect(res.status).to.equal(200);
      // expect(res.body.length).to.equal(2);
    });
  });


  describe("GET/:id", () => {
    it("should return a user if valid id is passed", async () => {
      const course = new courseModel({
        _id: new mongoose.Types.ObjectId(),
        title: "PHP",
        description: "Học PHP",
        noStudent: 10
      });
      console.log(`khoa hoc: ${course.id}`)
      await course.save();
      const res = await request(app).get("/courses/" + course._id);
      expect(res.status).to.equal(200);
      expect(res.body.data).to.have.property("title", "PHP");
    });

    it("should return 400 error when invalid object id is passed", async () => {
      const res = await request(app).get("/courses/1");
      expect(res.status).to.equal(400);
    });

    it("should return 404 error when valid object id is passed but does not exist", async () => {
      const res = await request(app).get("/courses/5f43ef20c1d4a133e4628181");
      expect(res.status).to.equal(404);
    });
  });

  describe("PUT /:id", () => {
    it("should update the existing course and return 200", async () => {
      const course = new courseModel({
        _id: new mongoose.Types.ObjectId(),
        title: "C++",
        description: "học C++",
        noStudent: 20
      });
      await course.save();

      const res = await request(app)
        .put("/courses/" + course._id)
        .send({
          title: "Angular",
          description: "học Angular",
          noStudent: 15
        });


      expect(res.status).to.equal(200);
      const courseUpdated = await courseModel.findOne({ _id: course._id });
      console.log(courseUpdated)
      expect(courseUpdated).to.have.property("title", "Angular");
      expect(courseUpdated).to.have.property("description", "học Angular");
      expect(courseUpdated).to.have.property("noStudent", 15);
    });
  });


  describe("DELETE /:id", () => {
    it("should delete requested id and return response 200", async () => {
      const course = new courseModel({
        _id: new mongoose.Types.ObjectId(),
        title: "HTML",
        description: "học HTML",
        noStudent: 10
      });
      await course.save();
      courseId = course._id;
      const res = await request(app).delete("/courses/" + courseId);
      expect(res.status).to.be.equal(204);
    });

    it("should return 404 when deleted course is requested", async () => {
      let res = await request(app).get("/courses/" + courseId);
      expect(res.status).to.be.equal(404);
    });
  });

})